# Используем официальный образ Node.js в качестве базового образа
FROM node:14

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем файлы package.json и package-lock.json
COPY package*.json ./

# Устанавливаем зависимости проекта
RUN npm install

# Копируем все файлы проекта в рабочую директорию контейнера
COPY . .

# Собираем проект React
RUN npm run build

# Указываем команду для запуска приложения React
CMD ["npm", "start"]
